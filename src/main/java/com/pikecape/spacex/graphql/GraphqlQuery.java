package com.pikecape.spacex.graphql;

import java.util.HashMap;
import java.util.Map;

/**
 * GraphQL query payload.
 *
 */
public class GraphqlQuery
{
    private String query;
    private Map<String, Object> variables = new HashMap<>();

    public GraphqlQuery() {
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Map<String, Object> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Object> variables) {
        this.variables = variables;
    }
}
