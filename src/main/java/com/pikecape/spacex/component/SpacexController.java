package com.pikecape.spacex.component;

import com.fasterxml.jackson.databind.JsonNode;
import com.pikecape.spacex.service.SpacexService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class SpacexController
{
    private final SpacexService spacexService;
    
    public SpacexController(SpacexService spacexService)
    {
        this.spacexService = spacexService;
    }

    /**
     * Get rockets.
     *
     * @param limit
     * @return ResponseEntity<>
     */
    @GetMapping(path = "/spacex", produces = {"application/json"})
    public @ResponseBody ResponseEntity<JsonNode> getRockets(@RequestParam("limit") int limit)
    {
        JsonNode response = spacexService.queryRockets(limit);

        if (response == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
    }
}
