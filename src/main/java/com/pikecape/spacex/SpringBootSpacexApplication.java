package com.pikecape.spacex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBootMockTestingApplication.
 *
 */
@SpringBootApplication
public class SpringBootSpacexApplication
{
    /**
     * Main.
     *
     * @param args
     */
    public static void main(String[] args)
    {
        SpringApplication.run(SpringBootSpacexApplication.class, args);
    }
}
