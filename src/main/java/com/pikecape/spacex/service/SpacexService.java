package com.pikecape.spacex.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.pikecape.spacex.graphql.GraphqlQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;

import static java.nio.charset.StandardCharsets.UTF_8;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SpacexService.
 *
 */
@Service
public class SpacexService
{
    @Autowired
    RestTemplate restTemplate;

    @Value("classpath:rocketQuery.txt")
    Resource rocketQuery;

    @Value("${spacex_api.url}")
    private String spacexApiUrl;

    // logger.
    private static final Logger LOGGER = LoggerFactory.getLogger(SpacexService.class);

    /**
     * Query rockets.
     *
     * @param limit
     * @return JsonNode
     */
    public JsonNode queryRockets(Integer limit) {
        // get query string.
        String rocketQueryString = this.getQueryString(this.rocketQuery);

        // check query string.
        if (rocketQueryString == null) {
            LOGGER.error("Rocket query failed.");
            return null;
        }

        // create request header.
        HttpHeaders requestHeader = new HttpHeaders();
        requestHeader.setContentType(MediaType.APPLICATION_JSON);
        requestHeader.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        // create graphql query; query string and variables.
        GraphqlQuery graphqlQuery = new GraphqlQuery();
        graphqlQuery.setQuery(rocketQueryString);
        graphqlQuery.getVariables().put("limit", limit);

        // create http entity; graphql query and request header.
        HttpEntity httpEntity = new HttpEntity(graphqlQuery, requestHeader);

        // call rest service.
        ResponseEntity<JsonNode> response = this.restTemplate.exchange(this.spacexApiUrl, HttpMethod.POST, httpEntity, JsonNode.class);

        // http status is not ok.
        if (!response.getStatusCode().equals(HttpStatus.OK)) {
            LOGGER.error("Rocket query failed.");
            return null;
        }

        // return graphql query response.
        return response.getBody();
    }

    /**
     * Get GraphQL query string from resource.
     *
     * @param queryResource
     * @return String
     */
    private String getQueryString(Resource queryResource)
    {
        String queryString;

        try {
            Reader queryReader = new InputStreamReader(queryResource.getInputStream(), UTF_8);
            queryString = FileCopyUtils.copyToString(queryReader);
            return queryString;
        } catch (IOException ioException) {
            LOGGER.error("Reading resource {} failed.", queryResource.getFilename());
            LOGGER.error(ioException.getMessage());
            return null;
        }
    }
}
