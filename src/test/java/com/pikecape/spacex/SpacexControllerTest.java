package com.pikecape.spacex;

import com.pikecape.spacex.configuration.SpacexConfiguration;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * SpacexControllerTest.
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {SpringBootSpacexApplication.class, SpacexConfiguration.class})
public class SpacexControllerTest
{
    @Value("${spacex_api.url}")
    private String spacexApiUrl;
    
    @Value("classpath:rocketQueryResponse.json")
    Resource rocketQueryResponse;

    @Autowired
    RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;
    
    @Before
    public void setUp()
    {
        this.mockServer = MockRestServiceServer.createServer(restTemplate);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    /**
     * Test of getRockets method, of class SpacexController.
     * @throws java.lang.Exception
     */
    @Test
    public void testSpacexController() throws Exception
    {
        mockServer
                .expect(requestTo(this.spacexApiUrl))
                .andExpect(method(HttpMethod.POST))
                .andExpect(MockRestRequestMatchers.header("Content-Type", MediaType.APPLICATION_JSON.toString()))
                .andRespond(withSuccess(rocketQueryResponse, MediaType.APPLICATION_JSON));

        this.mockMvc.perform(get("/spacex?limit=10"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(content().json(IOUtils.toString(rocketQueryResponse.getInputStream(), "UTF-8")));

    }
}
