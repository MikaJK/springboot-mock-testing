package com.pikecape.spacex;

import com.fasterxml.jackson.databind.JsonNode;
import com.pikecape.spacex.configuration.SpacexConfiguration;
import com.pikecape.spacex.service.SpacexService;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * SpacexServiceTest.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = {SpringBootSpacexApplication.class})
public class SpacexServiceTest
{
    @Value("${spacex_api.url}")
    private String spacexApiUrl;

    @Value("classpath:rocketQueryResponse.json")
    Resource rocketQueryResponse;

    @Autowired
    private RestTemplate restTemplate;

    @Configuration
    static class ContextConfiguration {
        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
    }

    @Autowired
    private SpacexService spacexService;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() throws Exception {
        this.mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    /**
     * Test of queryRockets method, of class SpacexService.
     */
    @Test
    public void testSpacexService()
    {
        mockServer
                .expect(requestTo(this.spacexApiUrl))
                .andExpect(method(HttpMethod.POST))
                .andExpect(MockRestRequestMatchers.header("Content-Type", MediaType.APPLICATION_JSON.toString()))
                .andRespond(withSuccess(rocketQueryResponse, MediaType.APPLICATION_JSON));

        JsonNode response = this.spacexService.queryRockets(10);
        
        assertNotNull(response);
        assertEquals(4, response.at("/data").get("rockets").size());
    }
}
